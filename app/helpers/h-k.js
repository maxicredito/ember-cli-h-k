import Ember from 'ember';

export function hK(hash, key) {
  return  hash && hash[key] ? hash[key] : '';
}

export default Ember.Handlebars.makeBoundHelper(hK);
