import Ember from 'ember';

export default Ember.ObjectController.extend({
  fields: ['name'],
});
