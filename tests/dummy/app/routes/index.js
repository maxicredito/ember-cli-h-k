import Ember from 'ember';

export default Ember.Route.extend({
  model: function() {
    return [
    { name: 'Paul', id: 1 },
    { name: 'Peter', id: 2}
    ];
  }
});
