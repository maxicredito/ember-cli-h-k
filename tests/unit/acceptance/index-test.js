import Ember from "ember";
import { test } from 'ember-qunit';
import startApp from '../../helpers/start-app';

var App;

module('Acceptance: h-k', {

});

test('Show value', function() {
  startApp();
  visit('/');

  andThen(function() {
    equal(find('ul li:last').text().trim(), 'Peter');
    equal(find('ul li:first').text().trim(), 'Paul');
  });
});
