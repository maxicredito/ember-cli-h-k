import {
  hK
} from '../../../helpers/h-k';

module('HKHelper');

test('test return value', function() {
  var result = hK({'name': 'Peter'}, 'name');
  equal('Peter', result);
});
